<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 6/02/19
 * Time: 17:25
 */

namespace ProyectoWeb\utils\Forms;


class ImgElement extends Element
{
    /**
     * @var string
     */
    private $src;
    /**
     * @var string
     */
    private $alt;
    /**
     * @var string
     */
    private $title;

    /**
     * ImgElement constructor.
     * @param string $src
     * @param string $alt
     * @param string $title
     */
    public function __construct(string $src, string $alt = '', string $title = '')
    {
        $this->src = $src;
        $this->alt = $alt;
        $this->title = $title;

        parent::__construct();
    }


    /**
     * Genera el código HTML del elemento
     *
     * @return string
     */
    public function render(): string
    {
        $html = "<img src='" . $this->src . "' alt='" . $this->alt . "' title='" . $this->title . "' " . $this->renderAttributes() . ">";
        return $html;
    }
}