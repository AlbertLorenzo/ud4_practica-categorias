<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 30/01/19
 * Time: 18:27
 */

namespace ProyectoWeb\utils\Validator;

use ProyectoWeb\utils\Validator\Validator;

class FileNotEmptyValidator extends Validator
{

    /**
     * Devuelve true si pasa la validación. False en caso contrario
     * Este método es el único que deben implementar todos los validadores.
     *
     * @return boolean
     */
    public function doValidate(): bool
    {
        $ok=($this->data["error"] === UPLOAD_ERR_OK);
        if(!$ok){
            $this->errors[] = $this->message;
        }
        return $ok;
    }
}